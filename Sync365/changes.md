﻿name|sysname|type
-|-|-
Опубликовать в tdm365|C_Publish_Project_365 |cmd
LIB_UTILITIES|LIB_UTILITIES |cmd
Библиотека для работы с XML|C_Library_XML|cmd
Отправить на рассмотрение|CMD_SEND_TO_365|cmd
Активирован в tdm365|A_Bool_Published_365|attr
Проверить статус проекта в tdm365|C_Check_Status_Project_365|cmd
Пакет выгрузки|A_Ref_Package_Unload|attr
Завершить работу с ЗМ|C_DocClaim_Processed|cmd
Замечание реестра|O_DocClaim|obj
Реестр замечаний|O_ClaimRegistry|obj